<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('orders', \App\Http\Controllers\Api\OrderController::class);

Route::get('vehicles', [\App\Http\Controllers\Api\VehicleController::class, 'index']);
Route::get('vehicles/{vehicle}', [\App\Http\Controllers\Api\VehicleController::class, 'show']);

Route::get('technicians', [\App\Http\Controllers\Api\TechnicianController::class, 'index']);

Route::get('keys', [\App\Http\Controllers\Api\KeyController::class, 'index']);
