<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Key Orders</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">


</div>
</body>
<body class="homepage en">
<div class="wrapper" id="app">
    <section class="page-container">
        <nav id="topban" class="regular">
            <div class="regular-area">
                <h2 class="area"></h2>

            </div>
            <div class="enter-area">
                <input type="text" class="subarea-input flatinput"
                       data-autocomplete="subarea" value="">
            </div>
            <div class="custom-area no-name ">
                <div class="radius-info">

                </div>
                <div class="exit-subarea">&times;</div>
            </div>
        </nav>
        <div id="leftbar">
            <h1 id="logo">
                <a href="/">Key Orders</a>
            </h1>
        </div>
        <div id="center">
            @yield('content')
        </div>
        <div id="rightbar">
        </div>
    </section>
    <footer>
        <ul class="clfooter">
            <li>&copy; 2021 <span class="desktop">Key Orders</span>
        </ul>
    </footer>

</div>

</body>

</html>
