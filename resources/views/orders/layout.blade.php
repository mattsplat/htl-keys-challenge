<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Key Orders</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>

<nav class="navbar navbar-expand-md navbar-light bg-primary fixed-top">
    <a class="navbar-brand" href="/">{{config('app.name')}}</a>
</nav>

<main role="main" class="container" id="app" style="margin-top: 8em;">
    <div class="row">
        <h2 class="mx-auto">Orders</h2>
    </div>
    @yield('content')
</main>
</body>
</html>
