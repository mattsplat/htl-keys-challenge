@extends('orders.layout')

@section('content')
    <order-form :order_id="{{$order->id}}"></order-form>
@endsection
