<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Technician;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $vehicle = Vehicle::inRandomOrder()->first();
        $key = $vehicle->keys()->inRandomOrder()->first();
        return [
            'technician_id' => Technician::inRandomOrder()->first()->id,
            'vehicle_id' => $vehicle->id,
            'key_id' => $key->id,
            'created_at' => now()->subDays(rand(0,364)),
        ];
    }
}
