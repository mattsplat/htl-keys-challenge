<?php

namespace Database\Seeders;

use App\Models\Key;
use App\Models\Order;
use App\Models\Technician;
use App\Models\Vehicle;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Key::insert(Key::factory(300)->raw());
        Vehicle::insert(Vehicle::factory(1000)->raw());
        Technician::insert(Technician::factory(50)->raw());

        $vehicles = Vehicle::get();
        $maxKey = Key::max('id');

        $vehicle_keys = $vehicles->flatMap(function ($vehicle) use ($maxKey) {
            $random_keys = [];
            for ($x = 0; $x < 3; $x++) {
                $random_key = rand(1, $maxKey);
                if (!in_array($random_key, $random_keys)) {
                    $random_keys[] = $random_key;
                }
            }

            return array_map(
                function ($key) use ($vehicle) {
                    return [
                        'key_id' => $key,
                        'vehicle_id' => $vehicle->id
                    ];
                },
                $random_keys
            );
        });

        $vehicle_keys->chunk(100)
            ->each(function ($chunk) {
                DB::table('key_vehicle')->insert($chunk->toArray());
            });

        Order::factory(100)->create();
    }
}
