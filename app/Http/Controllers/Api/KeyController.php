<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Key;
use Illuminate\Http\Request;

class KeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Key::query();

        if ($request->vehicle_id) {
            $query->whereHas(
                'vehicles',
                function ($q) use ($request) {
                    $q->where('vehicles.id', $request->vehicle_id);
                }
            );
        }

        if ($request->search) {
            $query->where('name', 'like', "{$request->search}%");
        }

        return $query->get();
    }

}
