<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderSearchRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderSearchRequest $request)
    {
        $query =  Order::with(['technician', 'vehicle', 'key']);

        $direction = $request->direction?: 'asc';

        $query->select('orders.*');
        // keyword search relations data
        if($request->search) {
            $query->whereHas('technician', function ($q) use ($request) {
                $q->where('first_name', 'like', "%{$request->search}%")
                    ->orWhere('last_name', 'like', "%{$request->search}%");
            })
                ->orWhereHas('key', function ($q) use ($request) {
                    $q->where('name', 'like', "%{$request->search}%");
                })
                ->orWhereHas('vehicle', function ($q) use ($request) {
                    $q->where('make', 'like', "%{$request->search}%")
                        ->orWhere('model', 'like', "%{$request->search}%");
                });
        }


        // order by relational data
        if ($request->sort) {
            switch ($request->sort) {
                case 'technician':
                    $query->join('technicians','orders.technician_id', '=', 'technicians.id')
                        ->orderBy('technicians.first_name', $direction);
                    break;
                case 'key':
                    $query->join('keys', 'orders.key_id', '=', 'keys.id')
                        ->orderBy('keys.name', $direction);
                    break;
                case 'vehicle':
                    $query->join('vehicles','orders.vehicle_id', '=', 'vehicles.id')
                        ->orderBy('vehicles.make', $direction);
                    break;
            }
        } else {
            $query->orderBy('orders.id', $direction);
        }

        return OrderResource::collection($query->paginate());;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        $order = Order::create($request->only('technician_id', 'key_id', 'vehicle_id'));

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return $order->load(['key', 'technician', 'vehicle']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOrderRequest $request, Order $order)
    {
        $order->update($request->only('technician_id', 'key_id', 'vehicle_id'));

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json('Success');
    }
}
