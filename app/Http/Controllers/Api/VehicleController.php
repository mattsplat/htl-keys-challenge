<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Vehicle::query();

        if ($request->search) {
            $query->where('make', 'like', "{$request->search}%")
                ->orWhere('model', 'like', "{$request->search}%");
        }

        if ($request->year) {
            $query->where('year',  $request->year);
        }

        if ($request->make) {
            $query->where('make',  $request->make);
        }

        if ($request->model) {
            $query->where('model',  $request->model);
        }

        if($request->vin) {
            $query->where('vin',  $request->vin);
        }

        if($request->get_column) {
            return $query->select($request->get_column)
                ->distinct($request->get_column)
                ->orderBy($request->get_column, 'asc')
                ->get()
                ->pluck($request->get_column);
        }

        return $query->limit(20)->get();
    }

    public function show(Request $request, Vehicle $vehicle)
    {
       return $vehicle;
    }

}
