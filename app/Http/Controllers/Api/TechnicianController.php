<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Technician;
use Illuminate\Http\Request;

class TechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Technician::query();


        if ($request->search) {
            $query->where('first_name', 'like', "{$request->search}%")
                ->orWhere('last_name', 'like', "{$request->search}%");
        }

        return $query->get();
    }

}
