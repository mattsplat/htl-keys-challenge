<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_id' => ['required', 'exists:vehicles,id'],
            'key_id' => [
                'required', 'exists:keys,id',
                function ($attribute, $value, $fail) {
                    $belongs_to_vehicle = DB::table('key_vehicle')
                        ->where('vehicle_id', request()->vehicle_id)
                        ->where('key_id', $value)
                        ->exists();
                    if(!$belongs_to_vehicle) {
                        $fail('The key does not belong to the vehicle.');
                    }
                },
            ],
            'technician_id' => ['required', 'exists:technicians,id']
        ];
    }
}
